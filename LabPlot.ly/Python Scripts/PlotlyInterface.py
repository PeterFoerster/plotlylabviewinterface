"""
Copyright (c) 2020 Foerster.tech, Peter Foerster
"""
import subprocess
import sys
import json
import os
import numpy as np
import win32api
# import chart_studio # for publishing
# import chart_studio.plotly as py # for publishing
import plotly.graph_objects as go
import lttb

def Reduce_Data(data, reduction):
    """
    Use the lttb.downsample function to reduce a dataset.

    Paramters
    ---------
    data: an np.array containing data and timebase information

    reduction: parameter controlling which reduction should be applied.
        0: not at all
        1+: to the number of samples provided by the reduction parameter
        -1: smartly, e.g. half the dataset
    Returns
    -------
    downsampled dataset
    """
    if reduction == 0:
        return data
    elif reduction == -1:
        # Implement "smart" reduction
        return data
    else:
        if len(data) < reduction:
            return data
        result = lttb.downsample(data, reduction)
        return result

def check_dependencies():
    """
    checks if all packages used by PlotlyInterface are installed and installs them if missing
    Returns
    -------
    a message containing the progress of this check operation.
    """
    dependencies_list = ["numpy", "lttb", "plotly"] # extend list, when adding new modules
    message = "Success: \n"
    for dependency in dependencies_list:
        try:
            message += "Attempting to import " + str(dependency) + "\n"
            __import__(dependency)
            message += "Success\n"
        except:
            return "Error: " + dependency
            # message += "Couldn't find dependency, attempting install...\nResult "
            # result = subprocess.check_call([sys.executable, "-m", "pip", "install", dependency])
            # message += str(result)
            # message += "\nPackage installed\n"
    return message

def install_dependency(name):
    subprocess.check_call([sys.executable, "-m", "pip", "install", name])


def Reduce_Data_Wrap(data, reduction):
    # for testing purposes only
    import win32api
    win32api.MessageBox(0, str(data), 'title')
    data_arr = np.array(data).T
    win32api.MessageBox(0, str(data_arr), 'title')
    return Reduce_Data(data_arr, reduction)


def Plot_2D_Multi(data, path, reductions, graphSettings, autoOpen):
    """
    Plots multiple 2D arrays on the same scatter plot
    the length of data, reductions and graphSettings need to match for all plots to print.
    Parameters
    ----------
    data: a list of tuples of 2D list, which contains the data. Each tuple in the list will be its own plot
    reductions: a list of number for the reduction setting
    graphSettings: a JSON string containing the look of each graph
    autoOpen: A boolean value that defines whether or not the graph opens after saving
    Returns
    -------
    a message indicating if the plot was successful
    """
    try:
        # Publishing Data
        #username = 'jak888'
        #api_key = 'vX66F1CKiWfx1davaRD1'
        #chart_studio.tools.set_credentials_file(username=username, api_key=api_key)


        # convert json to dict
        graphSettings = json.loads(graphSettings)

        # create figure
        fig = go.Figure()
        for (dataset, reduction, graphSetting) in zip(data, reductions, graphSettings):
            (dataset, *_) = dataset # free dataset from the tuple container

            # check if dataset is an np.array, since that's what lttb.downsample needs
            if isinstance(dataset, list):
                dataset = np.array(dataset).T
            
            dataset = Reduce_Data(dataset, reduction).T
            # populate the figure with scatter plots
            fig.add_trace(go.Scatter(
                x=dataset[0], y=dataset[1], mode=graphSetting['mode'], line=graphSetting['line']))
        if path:
            fig.write_html(path, auto_open=autoOpen)
        else:
            # display the figure at 127.0.0.1
            fig.show()

            # Publish to chart_studio
            #py.plot(fig, filename= '2D Lissajous', auto_open=True)
        return "Success"
    except Exception as e:
        return str(e)

def Plot_2D(data, path, reduction, graphSettings, autoOpen):
    """
    Plots a single 2D list on a scatter plot
    Parameters
    ----------
    data: a 2D list which contains data
    reduction: a parameter that controls which data reduction will be applied to the set
    graphSettings: a JSON string containing the look of the graph
    autoOpen: A boolean value that defines whether or not the graph opens after saving
    """
    # format data correctly, so Plot_2D_Multi can process it
    data = [(data,)] # list of tuples of data (owing to the cluster in LabVIEW)
    reduction = [reduction] # list of reduction
    return Plot_2D_Multi(data, path, reduction, graphSettings, autoOpen)


def Plot_1D(data, path, reduction, graphSettings, autoOpen):
    """
    Plots a single 1D list on a scatter plot
    Parameters
    ----------
    data: a 1D list which contains data
    reduction: a parameter that controls which data reduction will be applied to the set
    graphSettings: a JSON string containing the look of the graph
    autoOpen: A boolean value that defines whether or not the graph opens after saving
    """
    # generate an index and convert to a numpy array
    x = [float(x) for x in range(len(data))]
    XY_Data = [x, data]
    XY_Data = np.array(XY_Data).T
    return Plot_2D(XY_Data, path, reduction, graphSettings, autoOpen)

def Plot_Wfm(data, path, reduction, graphSettings, autoOpen):
    import datetime
    from datetime import timedelta
    try:
        obj = json.loads(data)
        time_list = []
        if reduction == 0:
            date_time_obj = datetime.datetime.fromisoformat(obj['t0'])
            time_list = [date_time_obj+datetime.timedelta(seconds=obj['dt']*i) for i in range(len(obj['y']))]
        else:
            time_list = [obj['dt']*i for i in range(len(obj['y']))]
        XY_Data =[time_list, obj['y']]
        Plot_2D(XY_Data, path, reduction, graphSettings, autoOpen)
        return "success"
    except Exception as e:
        return str(e)